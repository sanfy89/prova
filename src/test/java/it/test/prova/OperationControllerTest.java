package it.test.prova;

import it.test.prova.model.Addendi;
import org.apache.tomcat.util.file.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static net.bytebuddy.matcher.ElementMatchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@WebMvcTest(OperationController.class)
public class OperationControllerTest {

    @Autowired
    MockMvc mvc;

    @Test
    public void testOperationIntegerController() throws Exception {

        String addendi = "{\n" +
                "\"addendumA\" : 5, \n" +
                "\"addendumB\" : 6\n" +
                "}";

            mvc.perform(MockMvcRequestBuilders.post("/sum")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON).content(addendi))
                    .andExpect(status().isOk())
                    .andExpect(content()
                            .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                    .andExpect( content().json("{\n" +
                            "    \"result\": 11\n" +
                            "}"));



    }


    @Test
    public void testOperationIntegerControllerNull() throws Exception {

        String addendi = "{\n" +
                "\"valueA\" : 5, \n" +
                "\"addendumB\" : 6\n" +
                "}";

        mvc.perform(MockMvcRequestBuilders.post("/sum")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON).content(addendi))
                .andExpect(status().is4xxClientError());



    }



}
